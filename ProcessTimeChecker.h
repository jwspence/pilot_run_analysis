#include<stdlib.h>
#include<stdio.h>
#include<vector>
#include<time.h>
#include<TStopwatch.h>

class ProcessTimeChecker {
	
	private:
	int running;
	std::vector <char *> names;
	std::vector <double> realTime;
	TStopwatch sw;
	
	public:
	ProcessTimeChecker(){running=1;}
	void AddEntry(const char *name){ 
		if(names.size()!=0){
			sw.Stop();
			realTime.push_back(sw.RealTime());
		}
		int n = strlen(name);
		char * buf = new char[n+2];
		sprintf(buf,"%s",name);
		names.push_back(buf);
		sw.Start();
	};
	
	void End(){	AddEntry("End"); running=0;}
	
		
	void Print(){
		if(running) AddEntry("End");
		for(int i=0; i<names.size()-1; i++){
			printf("%-15s %7.2f\n", names[i], realTime[i]);
		}
	}
	
};

