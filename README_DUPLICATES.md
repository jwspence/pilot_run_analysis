# README to reproduce duplicate BTIDs

git clone https://gitlab.cern.ch/jwspence/pilot_run_analysis.git

source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/5.34.36/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh

cd pilot_run_analysis/Fedra

./install.sh

# Compile debug script

./compiledebug.sh

# Copy to destination

cp ./debugunique /eos/project-f/fasernu-pilot-run/Data/TS2/TS2Pb/TS2Pb_full/area00/

# Run on plate 90 only

./debugunique lnk0.def debug.root

This file contains only BTs in plate 90 of area00. Note that there are duplicates, i.e., different BTs with the same ID. This is NOT a consequence of the BT being in multiple areas because we are reading only area00.

