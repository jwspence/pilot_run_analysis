void combinemc(TString filename,double xmin,double xmax,double ymin,double ymax){
	TFile g(filename,"RECREATE");
	TNtuple *ntwrite = new TNtuple("ntuple","","x:y:z:iz:tx:ty:id:clstruth:etruth");
	for(int idx=1;idx<11;idx++){
		printf("Mu- file %d\n",idx);
		TFile f(Form("muminus%d.root",idx));
		TNtuple *nt = (TNtuple*)f.Get("ntuple");
		TLeaf *l = nt->FindLeaf("x");
		int nentries = nt->GetEntries();
		for(int i=0;i<nentries;i++){
			nt->GetEntry(i);
			double x = l->GetValue(0);
			if(x<xmin||x>xmax)continue;
			double y = l->GetValue(1);
			if(y<ymin||y>ymax)continue;
			if(i%100000==0)printf("%d / %d\n",i,nentries);
			ntwrite->Fill(1000*x,1000*y,1000*l->GetValue(2),l->GetValue(3),l->GetValue(4),l->GetValue(5),l->GetValue(6),l->GetValue(7),l->GetValue(8));
		}
		f.Close();
	}
	for(int idx=1;idx<11;idx++){
		printf("Mu+ file %d\n",idx);
		TFile f(Form("muplus%d.root",idx));
		TNtuple *nt = (TNtuple*)f.Get("ntuple");
		TLeaf *l = nt->FindLeaf("x");
		int nentries = nt->GetEntries();
		for(int i=0;i<nentries;i++){
			nt->GetEntry(i);
			double x = l->GetValue(0);
			if(x<xmin||x>xmax)continue;
			double y = l->GetValue(1);
			if(y<ymin||y>ymax)continue;
			if(i%100000==0)printf("%d / %d\n",i,nentries);
			ntwrite->Fill(1000*x,1000*y,1000*l->GetValue(2),l->GetValue(3),l->GetValue(4),l->GetValue(5),l->GetValue(6),l->GetValue(7),l->GetValue(8));
		}
		f.Close();
	}
	g.cd();
	ntwrite->Write();
	g.Close();
}
