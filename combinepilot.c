void combinepilot(TString filename){
	TFile g(filename,"RECREATE");
	TNtuple *ntwrite = new TNtuple("ntuple","","x:y:z:iz:tx:ty:id");
	for(int idx=1;idx<6;idx++){
		printf("Data file %d\n",idx);
		TFile f(Form("%d.root",idx));
		TNtuple *nt = (TNtuple*)f.Get("ntuple");
		TLeaf *l = nt->FindLeaf("x");
		int nentries = nt->GetEntries();
		for(int i=0;i<nentries;i++){
			nt->GetEntry(i);
			if(i%100000==0)printf("%d / %d\n",i,nentries);
			ntwrite->Fill(l->GetValue(0),l->GetValue(1),l->GetValue(2),l->GetValue(3),l->GetValue(4),l->GetValue(5),l->GetValue(6));
		}
		f.Close();
	}
	g.cd();
	ntwrite->Write();
	g.Close();
}
