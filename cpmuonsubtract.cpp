#include"tracking.h"
#include"ProcessTimeChecker.h"
#include<EdbSegP.h>

#include<EdbPattern.h>
#include<EdbDataSet.h>
#include<TCut.h>
#include<TStopwatch.h>
#include<TEnv.h>
#include<TThread.h>

#include<TBranch.h>
#include<TLeaf.h>

#include<stdlib.h>
#include<vector>
#include<map>
#include<iterator>

std::vector<Node*> nodes;
class segnode : public Node{
	public:
	
	segnode(EdbSegP* s) {
		v[0] = s->Plate();
		v[1] = s->X();
		v[2] = s->Y();
		data = new EdbSegP(*s);
	};
	EdbSegP* getData() { return (EdbSegP*) data;}
	virtual int ID() { return ((EdbSegP*) data)->ID();}
};

void read_nodes_lnkdef(char* filename,char* linkfilename,char* outfilename) {
	TFile flink("linked_tracks.root");
	TTree *t = (TTree*)flink.Get("tracks");
	TBranch *b = t->FindBranch("s");
	TLeaf *xl = t->FindLeaf("eX");
	TLeaf *yl = t->FindLeaf("eY");
	TLeaf *zl = t->FindLeaf("eZ");
	TLeaf *txl = t->FindLeaf("eTX");
	TLeaf *tyl = t->FindLeaf("eTY");
	TLeaf *izl = t->FindLeaf("s.eScanID.ePlate");
	TLeaf *idl = t->FindLeaf("eID");
	int nentrieslink = b->GetEntries();
	std::vector<std::vector<int>> linkidtable;
	linkidtable.resize(29);
	for(int i=0;i<nentrieslink;i++){
		b->GetEntry(i);
		int llen = xl->GetLen();
		for(int j=0;j<llen;j++){
			int id = idl->GetValue(j);
			int iz = izl->GetValue(j);
			linkidtable[iz-90].push_back(id);
		}
		if(i%10000==0)printf("%d / %d\n",i,nentrieslink);
	}
	for(int i=0;i<linkidtable.size();i++)printf("linkidtable[%d].size() = %d\n",i,linkidtable[i].size());

	printf("nentries = %d\n",nentrieslink);
	TFile fout(outfilename,"RECREATE");
	TNtuple *ntwrite = new TNtuple("ntuple","","x:y:z:iz:tx:ty:id");
	EdbDataProc* dproc = new EdbDataProc(filename);
	dproc->InitVolume(0);
	EdbPVRec* pvr = dproc->GetPVR();
	printf("Read %s\n", filename);
	for (int i = 0, Npatt = pvr->Npatterns(); i < Npatt; i++) {
		std::map<int,std::vector<double>> btmap;
		EdbPattern* pat = pvr->GetPattern(i);
		EdbSegP* s0 = pat->GetSegment(0);
		int plate = s0->Plate();
		printf("Plate %d\n",plate);
		for (int j = 0, patN = pat->GetN(); j < patN; j++) {
			EdbSegP* s = pat->GetSegment(j);
			segnode* sn = new segnode(s);
			nodes.push_back(sn);
			btmap.insert(std::pair<int,std::vector<double>>{s->ID(),{s->X(),s->Y(),s->Z(),s->Plate(),s->TX(),s->TY()}});
		}
		for(int j=0;j<linkidtable[i].size();j++)btmap.erase(linkidtable[plate-90][j]);
		int ct = 0;
		std::map<int,std::vector<double>>::iterator itr;
		for (itr = btmap.begin(); itr != btmap.end(); ++itr) {
			ct++;
			ntwrite->Fill(itr->second[0],itr->second[1],itr->second[2],itr->second[3],itr->second[4],itr->second[5],itr->first);
		}
		printf("ct = %d\n",ct);
	}
	delete pvr;
	fout.cd();
	ntwrite->Write();
	fout.Close();
}

int main(int argc, char* argv[]) {
	if (argc < 2) return 1;
	char* filename = argv[1];
	char* linkfilename = argv[2];
	char* outfilename = argv[3];
	TCut cut = "1";
	TString fname = filename;
	if (fname.Contains(".def")) {
		read_nodes_lnkdef(filename,linkfilename,outfilename);
	}
	for(int i=1000;i<8000;i++){
		segnode *sn = (segnode*)nodes[i];
		EdbSegP *s = sn->getData();
		float x = s->X();
		float y = s->Y();
		float z = s->Z();
		int iz = s->Plate();
		float tx = s->TX();
		float ty = s->TY();
		int id = s->ID();
		}
	
	return 0;
}

