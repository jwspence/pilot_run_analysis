#include"tracking.h"
#include"ProcessTimeChecker.h"
#include<EdbSegP.h>

#include<EdbPattern.h>
#include<EdbDataSet.h>
#include<TCut.h>
#include<TStopwatch.h>
#include<TEnv.h>
#include<TThread.h>

std::vector<Node*> nodes;

class segnode : public Node{
	public:
	
	segnode(EdbSegP* s) {
		v[0] = s->Plate();
		v[1] = s->X();
		v[2] = s->Y();
		data = new EdbSegP(*s);
	};
	EdbSegP* getData() { return (EdbSegP*) data;}
	virtual int ID() { return ((EdbSegP*) data)->ID();}
};

void read_nodes_lnkdef(char* filename) {
	EdbDataProc* dproc = new EdbDataProc(filename);
	dproc->InitVolume(0);
	EdbPVRec* pvr = dproc->GetPVR();
	printf("Read %s\n", filename);
	for (int i = 0, Npatt = pvr->Npatterns(); i < Npatt; i++) {
		EdbPattern* pat = pvr->GetPattern(i);
		//printf("ipat = %d\r", i);
		for (int j = 0, patN = pat->GetN(); j < patN; j++) {
			EdbSegP* s = pat->GetSegment(j);
			segnode* sn = new segnode(s);
			nodes.push_back(sn);
		}
	}
	delete pvr;	
}

int main(int argc, char* argv[]) {
	if (argc < 2) return 1;
	char* filename = argv[1];
	char* outfilename = argv[2];
	TCut cut = "1";
	TString fname = filename;
	TString outfname = outfilename;
	if (fname.Contains(".def")) {
		read_nodes_lnkdef(filename);
	}
	TFile g(outfilename,"RECREATE");
	TNtuple *nt = new TNtuple("ntuple","","x:y:z:iz:tx:ty:id");
	for(int i=0;i<nodes.size();i++){
		segnode *sn = (segnode*)nodes[i];
		EdbSegP *s = sn->getData();
		float x = s->X();
		float y = s->Y();
		float z = s->Z();
		int iz = s->Plate();
		float tx = s->TX();
		float ty = s->TY();
		int id = s->ID();
		printf("%d / %d %d %f %f %f %d %f %f\n",i,nodes.size(),id,x,y,z,iz,tx,ty);
		nt->Fill(x,y,z,iz,tx,ty,id);
	}
	nt->Write();
	g.Close();
	
	return 0;
}

