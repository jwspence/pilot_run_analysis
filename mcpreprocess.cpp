#include<TString.h>
#include<TFile.h>
#include<TTree.h>
#include<TBranch.h>
#include<TLeaf.h>
#include<TNtuple.h>
#include<stdlib.h>
int main(int argc,char* argv[]){
	TString fname = argv[1];
	TFile f(fname);
	TTree *t = (TTree*)f.Get("FASERnu");
	TBranch *b = t->FindBranch("row_wise_branch");
	int nentries = b->GetEntries();
	printf("nentries = %d\n",nentries);
	TLeaf *xl = b->FindLeaf("x");
	TLeaf *yl = b->FindLeaf("y");
	TLeaf *zl = b->FindLeaf("z");
	TLeaf *pxl = b->FindLeaf("px");
	TLeaf *pyl = b->FindLeaf("py");
	TLeaf *pzl = b->FindLeaf("pz");
	TLeaf *izl = b->FindLeaf("iz");
	TLeaf *izsubl = b->FindLeaf("izsub");
	TLeaf *chargel = b->FindLeaf("charge");
	TLeaf *idl = b->FindLeaf("pdgid");
	TString outfname = argv[2];
	TFile g(outfname,"RECREATE");
	TNtuple *nt = new TNtuple("ntuple","","x:y:z:iz:tx:ty:pdgid:clstruth:etruth");
	for(int i=0;i<nentries;i++){
		b->GetEntry(i);
		int llen = xl->GetLen();
		for(int j=0;j<llen;j++){
			int izsub = izsubl->GetValue(j);
			if(izsub!=0)continue; // Ignore hits in one layer of each emulsion plate
			int iz = izl->GetValue(j);
			if(iz<72)continue; // Take last 29 layers only
			int charge = chargel->GetValue(j);
			double x = xl->GetValue(j);
			double y = yl->GetValue(j);
			double z = zl->GetValue(j);
			double tx = pxl->GetValue(j)/pzl->GetValue(j);
			double ty = pyl->GetValue(j)/pzl->GetValue(j);
			double etruth = sqrt(pxl->GetValue(j)*pxl->GetValue(j)+pyl->GetValue(j)*pyl->GetValue(j)+pzl->GetValue(j)*pzl->GetValue(j));
			int id = idl->GetValue(j);
			if(i%1000==0)printf("%d %d %f %f %f %f %f %d %d\n",i,j,x,y,z,tx,ty,charge,id);
			nt->Fill(x,y,z,iz,tx,ty,id,i,etruth);
		}
	}
	g.cd();
	nt->Write();
	g.Close();
	f.Close();

}
