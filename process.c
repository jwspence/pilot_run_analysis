void processlinkedtracks(TString input,TString output){
	TFile f(input);
	TTree *t = (TTree*)f.Get("tracks");
	TBranch *b = t->FindBranch("s");
	TLeaf *xl = t->FindLeaf("eX");
	TLeaf *yl = t->FindLeaf("eY");
	TLeaf *zl = t->FindLeaf("eZ");
	TLeaf *izl = t->FindLeaf("s.eScanID.ePlate");
	TLeaf *txl = t->FindLeaf("eTX");
	TLeaf *tyl = t->FindLeaf("eTY");
	TLeaf *idl = t->FindLeaf("eID");
	TFile g(output,"RECREATE");
	TNtuple *ntwrite = new TNtuple("ntuple","","x:y:z:iz:tx:ty:id:trkid:nbts:ibt");
	int nentries = b->GetEntries();
	cout << nentries << " straight tracks" << endl;
	for(int trkid = 0;trkid<nentries;trkid++){
		b->GetEntry(trkid);
		int nbts = xl->GetLen();
		for(int ibt=0;ibt<nbts;ibt++){
			double x = xl->GetValue(0);
			double y = yl->GetValue(0);
			double z = zl->GetValue(0);
			int iz = izl->GetValue(0);
			double tx = txl->GetValue(0);
			double ty = tyl->GetValue(0);
			int id = idl->GetValue(0);	
			if(trkid%10000==0)cout << trkid << endl;
			ntwrite->Fill(x,y,z,iz,tx,ty,id,trkid,nbts,ibt);
		}
	}
	ntwrite->Write();
	g.Close();
	f.Close();
}
